<link rel="stylesheet" href="<?php echo URLROOT; ?>/css/style.css">
<nav class="navbar navbar-expand-lg navbar-dark mb-3" style="background-color: #003366;">




        <a class="navbar-brand" href="<?php echo URLROOT; ?>"><strong><?php echo SITENAME; ?></strong></a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="<?php echo URLROOT; ?>">Home</a>
            </li>


            <li class="nav-item">
                <a class="nav-link" href="<?php echo URLROOT; ?>/pages/about">About</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo URLROOT; ?>/pages/contact">Contact</a>
            </li>






        </ul>


        <form class="mx-2 my-auto d-inline w-50">
            <div class="input-group">
                <input type="text" class="form-control border border-right-0" placeholder="Search Food">
                <span class="input-group-append">
                    <button class="btn btn-outline-secondary border border-left-0" type="button">
                        <i class="fa fa-search" style="color:#00cc00;"></i>
                    </button>
                </span>
            </div>
        </form>







        <ul class="navbar-nav ml-auto">
            <?php if(isset($_SESSION['user_id'])) : ?>
                <li class="nav-item">
                    <a class="nav-link" href="#">Welcome <?php echo $_SESSION['user_name']; ?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo URLROOT; ?>/users/logout">Logout</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo URLROOT; ?>/users/profile/<?php echo $_SESSION['user_id']; ?>">Profile</a>
                </li>

            <?php else : ?>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo URLROOT; ?>/users/register">Register</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo URLROOT; ?>/users/login">Login</a>
            </li>
            <?php endif; ?>
        </ul>
    </div>
</div>
</nav>