<?php
class Food
{
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    // CSV to Database
    public function csvToDB($data)
    {
        // Prepare Query
        $this->db->query('INSERT INTO foods (FoodDescription, NutrientID, NutrientUnit, NutrientSymbol, FoodID, NutrientValue ) 
      VALUES (:FoodDescription, :NutrientID, :NutrientUnit, :NutrientSymbol, :FoodID, :NutrientValue)');

        // Bind Values
        echo $data['FoodDescription'];
        $this->db->bind(':FoodDescription', $data['FoodDescription']);
        $this->db->bind(':NutrientID', $data['NutrientID']);
        $this->db->bind(':NutrientUnit', $data['NutrientUnit']);
        $this->db->bind(':NutrientSymbol', $data['NutrientSymbol']);
        $this->db->bind(':FoodID', $data['FoodID']);
        $this->db->bind(':NutrientValue', $data['NutrientValue']);
        //$this->db->bind(':email', $data['email']);
        //$this->db->bind(':password', $data['password']);

        //Execute
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }
}