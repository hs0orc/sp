<?php

class Pages extends Controller {
    public function __construct() {
        // this->postModel = $this->model('Post'); // old, models/post was also deleted
    }

    public function index() {
        if(isLoggedIn()) {
            redirect('posts');
        }
        // $posts = $this->postModel->getPosts();

        $data = [
            // 'title' => 'FoodzDB',
            'title' => '',
            // 'description' => 'Personalized online training, nutrition and fitness'
            'description' => 'Nutrition information to meet your fitness goals'
            // 'description_extended' => 'Register to search for food'

           // 'posts' => $posts
            ];

        $this->view('pages/index', $data);
    }

    public function about() {
        $data = [
            'title' => 'About Us',
            'description' => 'App to share'
        ];
        $this->view('pages/about', $data);
    }


    public function contact() {
        $data = [
            'title' => 'Contact us',
            'description' => ''
        ];
        // sendEmail('camsimmonz@yahoo.com', 'China Doe', 'this is subject', 'this is body');
        $this->view('pages/contact', $data);
    }
}